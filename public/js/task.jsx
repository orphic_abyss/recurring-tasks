import React from "react";
import Button from "rsuite/Button";
import Container from "rsuite/Container";
import Form from "rsuite/Form";
import Panel from "rsuite/Panel";
import SelectPicker from "rsuite/SelectPicker";

const Group = Form.Group;
const Label = Form.ControlLabel;
const Control = Form.Control;

const colours = [
    "#34C3FF",
    "#3498FF",
    "#0052CC",
    "#429321",
    "#FFEB3B",
    "#F5A623",
    "#F44336",
    "#e91e63",
    "#9c27b0"
].map((item) => {
    return {
        label: item,
        value: item
    }
});

const pickerRenderValue = (value, item) => {
    return (
        <div><span style={{color: value}}>⬤</span> {value}</div>
    );
}

export default function Task (props) {
    const close = props.close;
    return (
        <Container>
            <Panel shaded>
                <Form fluid>
                    <Group>
                        <Label>Task Name</Label>
                        <Control name="name" type="text"/>
                    </Group>
                    <Group>
                        <Label>Repeat Frequency</Label>
                        <Control name="frequency" type="text"/>
                    </Group>
                    <Group>
                        <Label>Colour</Label>
                        <SelectPicker data={colours} block renderMenuItem={pickerRenderValue} renderValue={pickerRenderValue}/>
                    </Group>
                    <Button>Submit</Button>
                    <Button onClick={close}>Close</Button>
                </Form>
            </Panel>
        </Container>
    );
}
